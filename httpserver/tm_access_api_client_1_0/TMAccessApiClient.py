import requests
import requests.utils
import pickle
import json
from collections import namedtuple
from datetime import datetime, timedelta


Response = namedtuple("Response", "status_code text")


class HttpStatusCode(object):
    OK = 200
    BAD_REQUEST = 400
    UNAUTHORIZED = 401
    NO_CONTENT = 204
    SERVER_ERROR = 500
    # Not able to connect (ConnectionError)
    SERVER_DOWN = 503


class TMAccessApiClient(object):
    """
        Class responsible for sending client requests to TMAccessApi web server
    """

    def __init__(self, base_url, user_name, password, cookie_file_path, logger=None):
        """
            Default Constructor
            Dependency injection of logger to be done by caller
        """
        self.base_url = base_url
        self.user_name = user_name
        self.password = password
        self.cookie_file_path = cookie_file_path
        # self.logger = logger

        self.cookie_file_name = "TM_ACCESS_API.cookie"
        self.session = requests.Session()
        self.logged_in = False
        try:
            with open(self.cookie_file_path + self.cookie_file_name) as f:
                cookies = requests.utils.cookiejar_from_dict(pickle.load(f))
                self.session.cookies = cookies
                self.logged_in = True
        except IOError:
            pass

    def get_request(self, endpoint):
        url = self.base_url + endpoint
        # self.logger.info("Get Request to url: {}".format(url))
        try:
            response = self.session.get(url)
            return response
        except requests.exceptions.ConnectionError:
            return Response(status_code=HttpStatusCode.SERVER_DOWN, text="Server is Down")

    def post_request(self, endpoint, data):
        url = self.base_url + endpoint
        # self.logger.info(
        #     "Post Request to url: {} with data:{}".format(url, data))
        try:
            response = self.session.post(url, json=data)
            return response
        except requests.exceptions.ConnectionError:
            return Response(status_code=HttpStatusCode.SERVER_DOWN, text="Server is Down")

    def get_error_message(self, response):
        if response.status_code == HttpStatusCode.SERVER_ERROR:
            return "Server Error"
        try:
            return response.json()["message"]
        except AttributeError:
            return response.text

    def login(self):
        req_data = dict()
        req_data["email"] = self.user_name
        req_data["password"] = self.password

        response = self.post_request("/login", (req_data))

        if response.status_code == HttpStatusCode.OK:
            # save cookie everytime login request happens
            # self.logger.info("Request Successfull")
            with open(self.cookie_file_path + self.cookie_file_name, "wb") as f:
                pickle.dump(requests.utils.dict_from_cookiejar(
                    self.session.cookies), f)
        # else:
            # self.logger.error("ERROR CODE: {}, Error Message: {}".format(
            #     response.status_code, self.get_error_message(response)))

    def get_parameter_records(self, page_number, per_page_records, external_element, item_names, start_time, end_time, vcid):
        """
        Sample Request Body
        {
            "page_number": 1,
            "per_page": 100,
            "data_class":"STATE",
            "external_element":"HPC2",
            "parameter_list":["LAM_ENABLE"],
            "start_time":"2017-01-01 12:00:00",
            "end_time":"2017-12-01 12:00:00",
            "vcid":1
        }
        """

        req_data = dict()
        req_data["page_number"] = page_number
        req_data["per_page"] = per_page_records
        # To Be Fixed in TMAccessApi (No need for data_class)
        req_data["data_class"] = "STATE"
        req_data["external_element"] = external_element
        req_data["parameter_list"] = item_names
        req_data["start_time"] = str(start_time)
        req_data["end_time"] = str(end_time)
        req_data["vcid"] = vcid

        response = self.post_request("/getParameterRecords", req_data)
        # if response.status_code == HttpStatusCode.OK:
            # save cookie everytime login request happens
            # self.logger.info("Request Successfull")
        # else:
            # self.logger.error("ERROR CODE: {}, Error Message: {}".format(
            #     response.status_code, self.get_error_message(response)))
        return response

    def get_latest_tco_coeffs(self):
        """
        Retrieves latest TCO coefficients by calling api endpoint
        :return: Boolean(tells if api call was successful) and Dict of TCO coefficients
        """
        response = self.get_request("/getLatestTcoCoefficients")
        # if response.status_code == HttpStatusCode.OK:
        #     # save cookie everytime login request happens
        #     self.logger.info("Request Successfull")
        # else:
        #     self.logger.error("Error Code: {}, Error Message: {}".format(
        #         response.status_code, self.get_error_message(response)))
        return response
