import binascii
import time
import os
from main import *
from datetime import datetime
from metadataTM import roverMT

# Adding UTC epoch from 1/1/1970 to 1/1/2017 as a constant
EPOCH_CONSTANT = 1483228800000.0


def TMtoL1(rawTMM):
    timeStamp = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    data = rawTMM
    rawTM = []
    rawTM = bytearray(rawTMM)

    VERSION_NUMBER = (rawTM[0] & 0xE0) >> 5
    PCKT_TYPE = (rawTM[0] & 0x10) >> 4
    SEC_HDR_FLAG = (rawTM[0] & 0x08) >> 3
    APID = ((rawTM[0] & 0x07) << 8) | (rawTM[1])
    SEG_FLAG = ((rawTM[2] & 0xc0) >> 6)
    PCKT_SEQ_CNTR = ((rawTM[2] & 0x3F) << 8) | rawTM[3]
    DATA_LENGTH = (((rawTM[4]) << 8) | ((rawTM[5]))) - 4 + 1
    TIME_STAMP = (rawTM[6] << 24) | (
        rawTM[7] << 16) | (rawTM[8] << 8) | rawTM[9]

    # Adding UTC epoch from 1/1/1970 to EPOCH_CONSTANT as a constant. OnBoard sends in seconds resolution since 1/1/2017
    tm_pkt_ts = long(TIME_STAMP) * 1000 + long(EPOCH_CONSTANT)
    tm_pkt_ts = datetime.utcfromtimestamp(
        tm_pkt_ts / 1000.0).strftime('%Y-%m-%d %H:%M:%S')

    print "healthtm - apid <" + str(APID) + "> seqctr<" + str(PCKT_SEQ_CNTR) + "> datalen<" + str(DATA_LENGTH) + "> pktts<" + tm_pkt_ts + ">"

    rawtm_tx_power_raw = rawTM[10] & 0x7F
    rawtm_tx_power_raw_sign = ((rawTM[10]) & 0x80)
    if (rawtm_tx_power_raw_sign > 0):
        rawtm_tx_power_raw = rawtm_tx_power_raw * -1

    #rawtm_rssi = ((rawTM[12] << 8) | rawTM[11])
    rawtm_rssi = rawTM[11] & 0x7F
    rawtm_rssi_sign = ((rawTM[11]) & 0x80)
    if (rawtm_rssi_sign > 0):
        rawtm_rssi = rawtm_rssi * -1

    # skip 12th byte for now....fixing rssi to 1 byte

    rawtm_Wmotor_front_right_FB = ((rawTM[14] << 8) | rawTM[13])
    rawtm_Wmotor_rear_right_FB = ((rawTM[16] << 8) | rawTM[15])
    rawtm_Wmotor_front_left_FB = ((rawTM[18] << 8) | rawTM[17])
    rawtm_Wmotor_rear_left_FB = ((rawTM[20] << 8) | rawTM[19])
    rawtm_battery_voltage = ((rawTM[22] << 8) | rawTM[21])

    rawtm_acc_bf_meas_ele_x = (((rawTM[24] << 8) | rawTM[23]) & 0x7FFF)
    rawtm_acc_bf_meas_ele_x_sign = (((rawTM[24] << 8) | rawTM[23]) & 0x8000)
    if (rawtm_acc_bf_meas_ele_x_sign > 0):
        rawtm_acc_bf_meas_ele_x = rawtm_acc_bf_meas_ele_x * -1

    rawtm_acc_bf_meas_ele_y = ((rawTM[26] << 8) | rawTM[25]) & 0x7FFF
    rawtm_acc_bf_meas_ele_y_sign = (((rawTM[26] << 8) | rawTM[25]) & 0x8000)
    if (rawtm_acc_bf_meas_ele_y_sign > 0):
        rawtm_acc_bf_meas_ele_y = rawtm_acc_bf_meas_ele_y * -1

    rawtm_acc_bf_meas_ele_z = ((rawTM[28] << 8) | rawTM[27]) & 0x7FFF
    rawtm_acc_bf_meas_ele_z_sign = (((rawTM[28] << 8) | rawTM[27]) & 0x8000)
    if (rawtm_acc_bf_meas_ele_z_sign > 0):
        rawtm_acc_bf_meas_ele_z = rawtm_acc_bf_meas_ele_z * -1

    rawtm_REKF_Ang_axis_x = ((rawTM[30] << 8) | rawTM[29])
    rawtm_REKF_Ang_axis_y = ((rawTM[32] << 8) | rawTM[31])
    rawtm_REKF_Ang_axis_z = ((rawTM[34] << 8) | rawTM[33])
    rawtm_REKF_Bias_axis_x = ((rawTM[36] << 8) | rawTM[35])
    rawtm_REKF_Bias_axis_y = ((rawTM[38] << 8) | rawTM[37])
    rawtm_REKF_Bias_axis_z = ((rawTM[40] << 8) | rawTM[39])
    rawtm_Drive_flag = rawTM[41] & 0x01
    rawtm_EKF_UPDATE_FLAG = (rawTM[41] & 0x02) >> 1

    tm_tx_power = float(rawtm_tx_power_raw) * 0.204724409448819

    #tm_rssi = float(rawtm_rssi) * 0.00274666585283975
    tm_rssi = float(rawtm_rssi) * 0.204724409448819

    tm_Wmotor_front_right_FB = float(
        rawtm_Wmotor_front_right_FB) * 0.00479376310916273
    tm_Wmotor_rear_right_FB = float(
        rawtm_Wmotor_rear_right_FB) * 0.00479376310916273
    tm_Wmotor_front_left_FB = float(
        rawtm_Wmotor_front_left_FB) * 0.00479376310916273
    tm_Wmotor_rear_left_FB = float(
        rawtm_Wmotor_rear_left_FB) * 0.00479376310916273
    tm_battery_voltage = float(rawtm_battery_voltage) * 0.000305180437933928
    tm_acc_bf_meas_ele_x = float(
        (rawtm_acc_bf_meas_ele_x)) * 6.10370189519944E-05
    tm_acc_bf_meas_ele_y = float(
        rawtm_acc_bf_meas_ele_y) * 0.000122074037903989
    tm_acc_bf_meas_ele_z = float(
        rawtm_acc_bf_meas_ele_z) * 0.000305185094759972
    tm_REKF_Ang_axis_x = float(rawtm_REKF_Ang_axis_x) * 9.58767251683033E-05
    tm_REKF_Ang_axis_y = float(rawtm_REKF_Ang_axis_y) * 9.58767251683033E-05
    tm_REKF_Ang_axis_z = float(rawtm_REKF_Ang_axis_z) * 0.000191753450336607
    tm_REKF_Bias_axis_x = float(rawtm_REKF_Bias_axis_x) * 5.32547990356151E-06
    tm_REKF_Bias_axis_y = float(rawtm_REKF_Bias_axis_y) * 5.32547990356151E-06
    tm_REKF_Bias_axis_z = float(rawtm_REKF_Bias_axis_z) * 5.32547990356151E-06

    tm_Drive_flag = (rawtm_Drive_flag)
    tm_EKF_UPDATE_FLAG = (rawtm_EKF_UPDATE_FLAG)

    raw_TM_List = []
    raw_TM_List.append(rawtm_tx_power_raw)
    raw_TM_List.append(rawtm_rssi)
    raw_TM_List.append(rawtm_Wmotor_front_right_FB)
    raw_TM_List.append(rawtm_Wmotor_rear_right_FB)
    raw_TM_List.append(rawtm_Wmotor_front_left_FB)
    raw_TM_List.append(rawtm_Wmotor_rear_left_FB)
    raw_TM_List.append(rawtm_battery_voltage)
    raw_TM_List.append(rawtm_acc_bf_meas_ele_x)
    raw_TM_List.append(rawtm_acc_bf_meas_ele_y)
    raw_TM_List.append(rawtm_acc_bf_meas_ele_z)
    raw_TM_List.append(rawtm_REKF_Ang_axis_x)
    raw_TM_List.append(rawtm_REKF_Ang_axis_y)
    raw_TM_List.append(rawtm_REKF_Ang_axis_z)
    raw_TM_List.append(rawtm_REKF_Bias_axis_x)
    raw_TM_List.append(rawtm_REKF_Bias_axis_y)
    raw_TM_List.append(rawtm_REKF_Bias_axis_z)
    raw_TM_List.append(rawtm_Drive_flag)
    raw_TM_List.append(rawtm_EKF_UPDATE_FLAG)

    eu_TM_List = []
    eu_TM_List.append(tm_tx_power)
    eu_TM_List.append(tm_rssi)
    eu_TM_List.append(tm_Wmotor_front_right_FB)
    eu_TM_List.append(tm_Wmotor_rear_right_FB)
    eu_TM_List.append(tm_Wmotor_front_left_FB)
    eu_TM_List.append(tm_Wmotor_rear_left_FB)
    eu_TM_List.append(tm_battery_voltage)
    eu_TM_List.append(tm_acc_bf_meas_ele_x)
    eu_TM_List.append(tm_acc_bf_meas_ele_y)
    eu_TM_List.append(tm_acc_bf_meas_ele_z)
    eu_TM_List.append(tm_REKF_Ang_axis_x)
    eu_TM_List.append(tm_REKF_Ang_axis_y)
    eu_TM_List.append(tm_REKF_Ang_axis_z)
    eu_TM_List.append(tm_REKF_Bias_axis_x)
    eu_TM_List.append(tm_REKF_Bias_axis_y)
    eu_TM_List.append(tm_REKF_Bias_axis_z)
    eu_TM_List.append(tm_Drive_flag)
    eu_TM_List.append(tm_EKF_UPDATE_FLAG)

    print eu_TM_List
    for dicIdx in range(len(eu_TM_List)):
        roverMT[dicIdx]['timeStamp'] = tm_pkt_ts
        roverMT[dicIdx]['generation_time'] = timeStamp
        roverMT[dicIdx]['eu_value'] = eu_TM_List[dicIdx]
        roverMT[dicIdx]['raw_value'] = raw_TM_List[dicIdx]
    
    #print roverMT
        
    data = {
        "vcid": 3,
        "parameters": roverMT
    }

    #json_data = json.dumps(data, ensure_ascii=False, indent=3)
    #print json_data

    return data
