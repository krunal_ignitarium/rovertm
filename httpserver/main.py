import os.path
import time
import struct
import binascii
import requests
import json
import os
import sys
import binascii
from datetime import datetime
from postFile import *
from postTMtoL1 import *
from moonCast import MoonCast
from tm_access_api_client_1_0 import TMAccessApiClient
from threading import Lock

# Import CherryPy global namespace
import cherrypy
from cherrypy.lib import static


localDir = os.path.dirname(__file__)
absDir = os.path.join(os.getcwd(), localDir)

docBase = "/home/gsw/docbase"
PKT_COUNTER = 0
MAX_PACKETS_PER_FILE = 25
MOON_CAST_PATH = "/home/gsw/doccache/mooncast/eca-mooncast.264"
TMACCESSAPI_SERVER = "http://10.10.5.126"

mutexFileTM = Lock()
mutexHealthTM = Lock()
mutexMooncastTM = Lock()

class MyServer(object):
    def __init__(self):
        self.tmaccessapi_client = TMAccessApiClient(TMACCESSAPI_SERVER, "cg", "ab", ".", None)
        self.tmaccessapi_client.login()

    @cherrypy.expose
    def index(self):
        return '''
          <html>
          <h1>Rover Interface</h1>

          <h2>Capture Image</h2>
          <p>Issue POST request to /captureImage</p>
          <h3>Parameters:</h3>
          <ul>
            <li>cameraId: 1, 2, 3</li>
            <li>requestId: uint16</li>
          </ul>

          <h2>Get File</h2>
          <p>Issue GET request to /getFile</p>
          <h3>Parameters:</h3>
          <ul>
            <li>APID: uint16</li>
            <li>requestId: uint16</li>
          </ul>
        '''

    @cherrypy.expose
    def postFile(self):
	mutexFileTM.acquire()
        pktData = cherrypy.request.body.read()
        savePckt(pktData)
	mutexFileTM.release()
        
    @cherrypy.expose
    def pushMooncastFrame(self):
	mutexMooncastTM.acquire()
        # global mooncastPath
        data = cherrypy.request.body.read()
        MoonCast(data)
	mutexMooncastTM.release()
	return "Received"

    @cherrypy.expose
    def TestTM(self):
        data = cherrypy.request.body.read()
        print data

    @cherrypy.expose
    def postTM(self):
	mutexHealthTM.acquire()
        headers = {
            "Content-Type": "application/json",
            "Body": "binary"
        }

        rawTM = cherrypy.request.body.read()
        # print binascii.hexlify(rawTM)
        data = TMtoL1(rawTM)
        #print data
        # posting to L1 database
        response = self.tmaccessapi_client.post_request("/writeTelemetryParameters", data)
        print "got response from l1 db", response.status_code
        
        if (response.status_code != 200):
            print "failed to write to L1 db. status_code:", response.status_code

	mutexHealthTM.release()
        return ""


if __name__ == '__main__':
    serverconf = os.path.join(os.path.dirname(__file__), 'server.conf')
    cherrypy.config.update({
    'global': {
       'engine.autoreload.on' : False
     }
    })
    cherrypy.quickstart(MyServer(), config=serverconf)
    if (not os.path.exists(MOON_CAST_PATH)):
      os.mkfifo(MOON_CAST_PATH)

