import json
from datetime import datetime

timeStamp = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

roverMT = [
    {'external_element': 'rover', 'item_name': 'txPower',               'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units': 'DB',     'Resolution': 0.204724409449},
    {'external_element': 'rover', 'item_name': 'RSSI',                  'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'DB',    'Resolution': 0.204724409449},
    {'external_element': 'rover', 'item_name': 'Wmotor_front_right_FB', 'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'RDPS',  'Resolution': 0.00479376310916},
    {'external_element': 'rover', 'item_name': 'Wmotor_rear_right_FB',  'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'RDPS',  'Resolution': 0.00479376310916},
    {'external_element': 'rover', 'item_name': 'Wmotor_front_left_FB',  'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'RDPS',  'Resolution': 0.00479376310916},
    {'external_element': 'rover', 'item_name': 'Wmotor_rear_left_FB',   'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'RDPS',  'Resolution': 0.00479376310916},
    {'external_element': 'rover', 'item_name': 'battery_voltage',       'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'V',     'Resolution': 0.000305180437934},
    {'external_element': 'rover', 'item_name': 'acc_bf_meas_ele_x',     'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'MPS2',  'Resolution': 6.1037018952e-05},
    {'external_element': 'rover', 'item_name': 'acc_bf_meas_ele_y',     'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'MPS2',  'Resolution': 0.000122074037904},
    {'external_element': 'rover', 'item_name': 'acc_bf_meas_ele_z',     'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'MPS2',  'Resolution': 0.00030518509476},
    {'external_element': 'rover', 'item_name': 'REKF_Ang_axis_x',       'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'RAD',   'Resolution': 9.58767251683e-05},
    {'external_element': 'rover', 'item_name': 'REKF_Ang_axis_y',       'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'RAD',   'Resolution': 9.58767251683e-05},
    {'external_element': 'rover', 'item_name': 'REKF_Ang_axis_z',       'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'RAD',   'Resolution': 0.000191753450337},
    {'external_element': 'rover', 'item_name': 'REKF_Bias_axis_x',      'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'ANY',   'Resolution': 5.32547990356e-06},
    {'external_element': 'rover', 'item_name': 'REKF_Bias_axis_y',      'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'ANY',   'Resolution': 5.32547990356e-06},
    {'external_element': 'rover', 'item_name': 'REKF_Bias_axis_z',      'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  '000',   'Resolution': 5.32547990356e-06},
    {'external_element': 'rover', 'item_name': 'Drive_flag',            'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'ANY',   'Resolution': 1},
    {'external_element': 'rover', 'item_name': 'EKF_UPDATE_FLAG',       'generation_time': timeStamp, 'raw_value': 0, 'eu_value': 0, 'eu_units':  'ANY',   'Resolution': 1}
]
