import binascii
import time
import os
from main import *
from datetime import datetime

mooncastFile = "eca-mooncast.264"
mooncastDir = "/home/gsw/doccache/mooncast/"
mooncast_sd_file = "/home/gsw/doccache/mooncast/eca-mooncast-sd.264"

def MoonCast(data):
    VDO_PCKTS = []
    VDO_PCKTS = bytearray(data)
    VERSION_NUMBER = (VDO_PCKTS[0] & 0xE0) >> 5
    PCKT_TYPE = (VDO_PCKTS[0] & 0x10) >> 4
    SEC_HDR_FLAG = (VDO_PCKTS[0] & 0x08) >> 3
    APID = ((VDO_PCKTS[0] & 0x07) << 8) | (VDO_PCKTS[1])
    SEG_FLAG = ((VDO_PCKTS[2] & 0xc0) >> 6)
    PCKT_SEQ_CNTR = ((VDO_PCKTS[2] & 0x3F) << 8) | VDO_PCKTS[3]
    DATA_LENGTH = (((VDO_PCKTS[4]) << 8) | ((VDO_PCKTS[5]))) + 1
    REQUEST_ID = (VDO_PCKTS[6] << 8) | VDO_PCKTS[7]
    PCKT_DATA_CNTR = (VDO_PCKTS[8] << 8) | VDO_PCKTS[9]
    VDO_DATA = VDO_PCKTS[10:(10 + DATA_LENGTH)]

    #print "Pckt Length          : ", len(data)
    #print "Header               : ", binascii.hexlify(data[0:10])
    #print "Version Number       : ", VERSION_NUMBER
    #print "Packet Type          : ", PCKT_TYPE
    #print "Secondery HDR Flag   : ", SEC_HDR_FLAG
    #print "Application ID       : ", APID
    #print "Seq Flag             : ", SEG_FLAG
    #print "Pckt Seq Cnt         : ", PCKT_SEQ_CNTR
    #print "Data Length          : ", DATA_LENGTH
    #print "Request ID           : ", REQUEST_ID
    #print "Pckt Data Cnt        : ", PCKT_DATA_CNTR
    #print "Stored Data Length   : ", len(VDO_DATA)
    print "mooncast - apid: " + str(APID)+ " ReqId: " + str(REQUEST_ID) + " Seq Flg: " + str(SEG_FLAG) + " Seq Ctr: " + str(PCKT_DATA_CNTR)
    
    
    # os.mkfifo(path)

    print "Moon cast", SEG_FLAG
    if SEG_FLAG == 0:
    	f1 = open(mooncast_sd_file, "ab")
    	fifo = open(mooncastDir + mooncastFile, "wb+")
        print "Opened"
    	fifo.write(VDO_DATA)
    	f1.write(VDO_DATA)
        print "Written"
    	f1.close()
        fifo.close() 
        print "Closed"
    elif SEG_FLAG == 1:
	    #print "Begining of MoonCast Frame", str(REQUEST_ID)
    	f1 = open(mooncast_sd_file, "wb")
        print "Opened"
    	fifo = open(mooncastDir + mooncastFile, "wb+")
    	fifo.write(VDO_DATA)
    	f1.write(VDO_DATA)
        print "Written"
    	f1.close()
        print "Closed"
        fifo.close() 
    elif SEG_FLAG == 2:
        print "End of MoonCast Frame", REQUEST_ID
    	f1 = open(mooncast_sd_file, "ab")
    	fifo = open(mooncastDir + mooncastFile, "wb")
        fifo.write(VDO_DATA)
    	f1.write(VDO_DATA)

    	f1.close()
        fifo.close() 
        #os.chdir(mooncastDir)
        #os.remove(mooncastFile)
    print "END OF MOONCAST"

    #print "End of write"

