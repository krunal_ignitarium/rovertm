import binascii
import time
import os
from main import *
from datetime import datetime
from metadataTM import roverMT


def savePckt(pktData):

    global PKT_COUNTER
    global MAX_PACKETS_PER_FILE
    path = "/home/gsw/doccache/"

    timeStamp = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    data = pktData
    rawTM = []
    rawTM = bytearray(pktData)
    VERSION_NUMBER = (rawTM[0] & 0xE0) >> 5
    PCKT_TYPE = (rawTM[0] & 0x10) >> 4
    SEC_HDR_FLAG = (rawTM[0] & 0x08) >> 3
    apid = ((rawTM[0] & 0x07) << 8) | (rawTM[1])
    SEG_FLAG = ((rawTM[2] & 0xc0) >> 6)
    PCKT_SEQ_CNTR = ((rawTM[2] & 0x3F) << 8) | rawTM[3]
    DATA_LENGTH = (((rawTM[4]) << 8) | ((rawTM[5]))) - 4 + 1
    FileRequestId = (rawTM[6] << 8) | rawTM[7]
    FileSeqCounter = (rawTM[8] << 8) | rawTM[9]

    print "filetm - apid<" + str(apid) + "> reqId<" + str(FileRequestId) + "> FileSeqCtr<" + str(FileSeqCounter) + "> SeqFlg<" + str(SEG_FLAG) + ">"

    # year #days #hr #min #sec #millis(only first 3 char)
    time_string = datetime.utcnow().strftime("%Y_%j_%H_%M_%S_%f")
    packet_file_name = "" + time_string + "_apid_" + str(apid) + ".pkts"
    metadata_file_name = packet_file_name + ".meta_data"
    time_ms = int(round(time.time() * 1000000))
    metadata = "0,0,3,0," + str(time_ms) + "\n"

    if SEG_FLAG == 1:
        fp = open(path + "RawFile", 'ab')
        fp.write(pktData)

        mfp = open(path + "RawMetadataFile", 'a')
        mfp.write(metadata)

        PKT_COUNTER += 1
    elif SEG_FLAG == 0:
        fp = open(path + "RawFile", 'ab')
        fp.write(pktData)

        mfp = open(path + "RawMetadataFile", 'a')
        mfp.write(metadata)

        PKT_COUNTER += 1

        if (PKT_COUNTER >= MAX_PACKETS_PER_FILE):
            fp.close()
            mfp.close()

            os.chdir(path)
            if (os.path.isfile(path+"RawFile")):
                os.rename("RawFile", packet_file_name)
            if (os.path.isfile(path+"RawMetadataFile")):
                os.rename("RawMetadataFile", metadata_file_name)

            PKT_COUNTER = 0
    elif SEG_FLAG == 2:

        fp = open(path + "RawFile", 'ab')
        fp.write(pktData)

        mfp = open(path + "RawMetadataFile", 'a')
        mfp.write(metadata)
        fp.close()
        mfp.close()

        os.chdir(path)
        os.rename('RawFile', packet_file_name)
        os.rename('RawMetadataFile', metadata_file_name)
    elif SEG_FLAG == 3:
        mdFP = open(path + "md", 'ab')
        mdFP.write(pktData)
        mfp = open(path + "RawMetadataFile", 'a')
        mfp.write(metadata)

        os.chdir(path)
        os.rename('md', packet_file_name)
        os.rename('RawMetadataFile', metadata_file_name)

        PKT_COUNTER += 1

    else:
        # TODO: nothing to do. skip
        print "Invalid SEG_FLAG, skipping pkt"

    if SEG_FLAG == 1:
        xx = open(path + "Image", 'ab')
        xx.write(pktData[10:])
    elif SEG_FLAG == 0:
        xx = open(path + "Image", 'ab')
        xx.write(pktData[10:])
    elif SEG_FLAG == 2:
        xx = open(path + "Image", 'ab')
        xx.write(pktData[10:])
        xx.close()

        os.chdir(path)
        os.rename('Image', packet_file_name + ".jpeg")
    else:
        print

    return "Done"
